#include "operations.h"
#include "serverdatacenter.h"
#include "databaseoperation.h"

Operations::Operations(QObject *parent) : QObject(parent) {

}

ServerDataCenter& dcenter = ServerDataCenter::Singleton();
using resp = QList<QJsonObject>;
using Json = QJsonObject;

resp Operations::loginResponse(QJsonObject json) {
    DatabaseOperation & db = DatabaseOperation::Singleton();
    QString username = json["Username"].toString();
    QString password = json["Password"].toString();

    Json response = { {"MsgType", "LogInConfirm"}, {"IsLegal", false}};
    if (!db.attemptLogIn(username, password)) {
        return {response};
    }
    response["IsLegal"] = true;
    response["Username"] = username;

    auto & user = dcenter.getUser(username);
    response["Nickname"] = user.getNickname();
    response["Profile"] = user.getProfile();
    resp ret = {response};

    auto sessionlist = db.querySessionsByMember(username.toUtf8().data());
    for (int i = 0; i < sessionlist.size(); i++) {
        int ssid = sessionlist.at(i);
        auto userlist = db.queryMembersBySession(ssid);
        for (int j = 0; j < userlist.size(); j++) {
            auto cur = userlist.at(j);
            if (cur == username) continue;

            auto & curuser = dcenter.getUser(cur);
            ret.append(curuser.generateUserModelJson());

        }
        ret.append(dcenter.getSession(ssid).generateJsonFromData());
        auto msglist = db.queryMessageBySession(sessionlist.at(i));
        for (int j = 0; j < msglist.size(); j++) {
            try{
                ret.append(dcenter.getMessage(ssid, msglist[j]).generateJsonOutput());
            } catch (...) {
                qDebug() << "fatal error";
            }
        }
        ret.append(dcenter.getSession(sessionlist.at(i)).generateJsonFromData());
    }
    return ret;
}


resp Operations::registerResponse(QJsonObject json) {
    DatabaseOperation & db = DatabaseOperation::Singleton();
    auto & dcenter = ServerDataCenter::Singleton();
    Json head = {{"MsgType", "RegistConfirm"},
                {"IsLegal", false}};
    if (dcenter.hasUser(json["Username"].toString())) return {head};
    QString username = json["Username"].toString();
    qDebug() << "Registering User into DB, username = " << username;
    QString nickname = json["Nickname"].toString();
    QString password = json["Password"].toString();
    
    if (!db.insertUser(username, nickname, password, QJsonObject({{"Signiture", "None"}})))
        return {head};
    qDebug() << "Register: Success";
    head["IsLegal"] = true;
    return {head};
}

resp Operations::newMessageResponse(QJsonObject json) {
    DatabaseOperation & db = DatabaseOperation::Singleton();
    int sessionID = json["SessionID"].toInt();
    QString senderUsername = json["SenderName"].toString();
    auto json1 = json["Body"].toObject();
    QString text = json1["Text"].toString();
    bool mention = json1["Profile"].toObject()["hasMentionInfo"].toBool();
    if (mention) {
        throw "Not Implemented";
    }
    int msgID = db.insertNewMessage(sessionID, senderUsername, text, json["Profile"].toObject());
    json["MessageID"] = msgID;
    emit newMessage(sessionID, json);

    return resp();
}

resp Operations::addFriendResponse(QJsonObject json) {
    DatabaseOperation & db = DatabaseOperation::Singleton();
    QString from = json["FromUsername"].toString(), to = json["ToUsername"].toString();
    OnlineUserModel & fromUser = dcenter.getUser(from), &toUser = dcenter.getUser(to);

    auto sessionlist = db.querySessionsByMember(from.toUtf8().data());
    for (int i = 0; i < sessionlist.size(); i++) {
        auto & session = dcenter.getSession(sessionlist[i]);
        auto & members = session.getMembers();
        for (int j = 0; j < members.size(); j++) {
            if (members[j] == to && session.getSessionType() == AbstractSession::SessionType::FRIEND)
                return resp();
        }
    }

    int id = db.insertSessionBasicInfo("FRIEND", QJsonObject({{"LatestMessageID", 0}, {"SessionName", "None"}}));
    db.insertMember(id, fromUser);
    db.insertMember(id, toUser);

    auto response = dcenter.getSession(id).generateJsonFromData();

    emit newMessage(id, response);
    return resp();
}

resp Operations::searchResponse(QJsonObject json) {
    DatabaseOperation & db = DatabaseOperation::Singleton();
    ServerDataCenter & dcenter = ServerDataCenter::Singleton();
    QString queryStr = json["SearchInfo"].toString();

    QList<QString> result = db.findFriend(queryStr);
    QJsonObject response = {{ "MsgType", "SearchInfo" }};
    QJsonArray array;
    for (int i = 0; i < result.size(); i++) {
        array.append(QJsonObject({{"Username", result[i]},
                                  {"Nickname", dcenter.getUser(result[i]).getNickname()}}));
    }
    response["SearchInfo"] = array;
    return {response};
}

resp Operations::createGroupResponse(QJsonObject json) {
    DatabaseOperation & db = DatabaseOperation::Singleton();
    ServerDataCenter & dcenter = ServerDataCenter::Singleton();

    QString sessionName = json["SessionName"].toString();
    QJsonArray array = json["Members"].toArray();
    int id = db.insertSessionBasicInfo("GROUP",  QJsonObject({{"LatestMessageID", 0}, {"SessionName", sessionName}}));
    for (int i = 0; i < array.size(); i++) {
        QString username = array[i].toObject()["username"].toString();
        db.insertMember(id, dcenter.getUser(username));
    }

    QJsonObject response = dcenter.getSession(id).generateJsonFromData();
    emit newMessage(id, response);
    return resp();
}
