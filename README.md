# BICQ

#### 介绍
BIT ICQ, a Realtime Communicating Solution using C++ Qt framework

### 代码规范约定 与 提交说明
1.  类名、文件名使用大驼峰命名法，函数名使用小驼峰命名法
2.  仓库在 master 和 Develop 分支上设置了保护，一般工作在feature分支上，如果是可以独立开发的模块，强烈建议单独拉一条新分支出来。每个 working state 会合并到 Develop 分支，release 版本合并到 master 分支

```json
{
    "MsgType": "JsonArray",
    "MsgList": [
        { ... },
        { ... },
        { ... },
    ]
}

```
### 加好友
```json
{
    "MsgType": "AddFriendRequest",
    "FromUsername": "...",
    "ToUsername": "..."
}
```

### 注册信息

  ``` json
  {
      "MsgType": "Regist",
      "Username": "...",
      "Nickname": "...",
      "Password": "..."
  }
  ```
注册信息确认
``` json
  {
      "MsgType": "RegistConfirm",
      "IsLegal":true/false,
  }
  ```

### 登录消息

  ``` json
  {
      "MsgType": "LogIn",
      "Username": "...",
      "Password": "..."
  }
  ```

登录消息确认
``` json
  {
      "MsgType": "LogInConfirm",
      "IsLegal":true,
      "Username": "...",
      "Nickname": "...",
      "Profile" : {
        "signiture" : "...",
        "..." : "...",
      }
  }
  ```
``` json
  {
      "MsgType": "LogInConfirm",
      "IsLegal": false,
      }
  }
  ```
### 搜索消息

  ``` json
  {
      "MsgType": "Search",
      "SearchInfo": "..."
  }
  ```

 ``` json
  {
      "MsgType": "SearchInfo",
      "SearchInfo": [
          { "UserName": "...", "NickName": "..." },
          { "UserName": "...", "NickName": "..." },
          ...
          { "UserName": "...", "NickName": "..." }
      ]
  }
  ```
### 新建群组消息
  ``` json
  {
      "MsgType": "NewGroup",
      "SessionName":"..."
      "Members": [
        { "username": "..." },
        { "username": "..." },
        { "username": "..." },
      ],
  }
  ```
### 登录后服务端行为

首先发送 LogInConfirm
接下来分别发送所有与该用户相关的 Session 信息， 相关 User 信息
客户端加载数据库，发送消息版本信息
服务端发送缺少的消息

- 消息同步码
``` json
{
    "MsgType": "MessageSyncCode",
    "SessionID": 1,
    "Latest Message ID": 123
}
```


### 数据格式协议

#### User Model 的 Json 表示
  ``` json
  {
      "MsgType": "UserData",
      "Username": "...",
      "Nickname": "...",
      "Profile" : {
        "signiture" : "...",
        "..." : "...",
      }
  }
  ```

#### 会话及会话关系

1. 好友会话
  ``` json
  {
      "MsgType": "SessionData",
      "SessionID": 111,
      "SessionType": "FRIEND", 
      "Members": [
        { "Username": "..." },
        { "Username": "..." },
      ],
      "Profile": [
        "LatestMessageID": ...,
      ]
  }
  ```

2. 群组会话
  ``` json
  {
      "MsgType": "SessionData",
      "SessionID": 112,
      "SessionType": "GROUP",
      "Latest Message ID":123 
      "Members": [
        { "username": "..." },
        { "username": "..." },
        { "username": "..." },
      ],
      "Profile": {
        "SessionName": "...",
        "LatestMessageID": ...,
      }
  }
  ``` 

3. 用户处于某一会话

``` json
{
    "MsgType": "UserInSessionRelationship",
    "Username": "...",
    "SessionID": 111,
}
``` 

#### 消息模型
``` json
  {
      "MsgType": "SessionMessage",
      "SessionID": 111,
      "MessageID": 1,
      "SenderName": "...",
      
      "Body": {
          "Text": "...",
          "Profile": {
            "...": "...",
          }
      }
  }
``` 
- 注：客户端向服务器发送时没有 "MessageID"



#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx


