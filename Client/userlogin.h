#ifndef USERLOGIN_H
#define USERLOGIN_H

#include <QWidget>
#include<QJsonObject>
#include<mainwindow.h>
#include<QTcpSocket>
#include<userregister.h>

QT_BEGIN_NAMESPACE
namespace Ui { class UserLogin; }
QT_END_NAMESPACE

class UserLogin : public QWidget
{
    Q_OBJECT

public:
    MainWindow *main = nullptr;
    UserLogin(QWidget *parent = nullptr);
    ~UserLogin();
    void loginconfirm(QJsonObject data);
signals:
    void registerButtonClicked();
    void sendlogindata(QJsonObject data);
    void createMainWindow(QJsonObject data);
private slots:

    void on_lineEditPassword_returnPressed();

private:
    Ui::UserLogin *ui;
};
#endif // USERLOGIN_H
