#ifndef USERREGISTER_H
#define USERREGISTER_H

#include <QWidget>
#include<QJsonArray>
#include<QJsonObject>

namespace Ui {
class UserRegister;
}

class UserRegister : public QWidget
{
    Q_OBJECT

public:
    explicit UserRegister(QWidget *parent = nullptr);
    ~UserRegister();
    void registconfirm(QJsonObject data);

signals:
    void registfinished();
    void sendregistdata(QJsonObject data);
private slots:

    void on_lERgPassword1_returnPressed();

private:
    Ui::UserRegister *ui;
};

#endif // USERREGISTER_H
