#include "userlogin.h"
#include "ui_userlogin.h"
#include<userregister.h>
#include<QJsonObject>
#include<qmessagebox.h>
#include<QDebug>
#include"mainwindow.h"
#include<QMessageBox>
#include<QHostAddress>
#include<QJsonDocument>
#include<clientmain.h>



UserLogin::UserLogin(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::UserLogin)
{
    ui->setupUi(this);

    //点击注册跳转到注册页面
    connect(ui->btnRegister,&QPushButton::clicked,[=](){
        this->hide();
        emit registerButtonClicked();
    });
//#define UIDEBUG
#ifndef UIDEBUG
    //点击登录发送登录信息
    connect(ui->btnLogIn,&QPushButton::clicked,[=](){
        QJsonObject login = { {"MsgType","LogIn"},{"Username",ui->lineEditUserName->text()},{"Password",ui->lineEditPassword->text()}};
        emit sendlogindata(login);
    });
#else
    //调试入口
    connect(ui->btnLogIn, &QPushButton::clicked,[=](){
        MainWindow *main = new MainWindow();
        main->show();
    });
#endif

    // main = new MainWindow(this);
}

void UserLogin::loginconfirm(QJsonObject data){
    if(data["IsLegal"].toBool()==false){
        QMessageBox::critical(this,"Error!","用户名或密码有误");
    }
    else{
        this->hide();
        emit createMainWindow(data);
    }
}

UserLogin::~UserLogin()
{
    delete ui;
}


void UserLogin::on_lineEditPassword_returnPressed()
{
    emit ui->btnLogIn->clicked();
}
